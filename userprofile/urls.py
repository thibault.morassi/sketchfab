from django.conf.urls import url
from django.views.generic import TemplateView

from userprofile import views


urlpatterns = [
    url(r'^$', views.profile_view, name='profile_view'),
    url(r'^signup/$', views.profile_new, name='profile_new'),
]

