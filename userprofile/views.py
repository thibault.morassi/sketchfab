from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect


@login_required
def profile_view(request):
    return render(request, 'userprofile/view.html')


def profile_new(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()

            # User created: now login
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('profile_view')
            else:
                return redirect('model3d_list')

    else:
         form = UserCreationForm()

    context = {
        'form': form,
    }
    return render(request, 'userprofile/new.html', context)

