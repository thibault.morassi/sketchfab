from datetime import date, datetime

from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.signals import user_logged_in
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from model3d.signals import sig_model3d_displayed, sig_model3d_added
from model3d.models import Model3d

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    starBadge = models.BooleanField('Badge star', default=False)
    collectorBadge = models.BooleanField('Badge collector', default=False)
    pioneerBadge = models.BooleanField('Badge pioneer', default=False)

    def __str__(self):
        return self.user.username

    def hasStarBadge(self):
        return self.starBadge

    def hasCollectorBadge(self):
        return self.collectorBadge

    def hasPioneerBadge(self):
        return self.pioneerBadge

    def getBadges(self):
        badges = []
        if self.hasStarBadge():
            badges.append("Star")
        if self.hasCollectorBadge():
            badges.append("Collector")
        if self.hasPioneerBadge():
            badges.append("Pioneer")
        return badges


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


@receiver(user_logged_in, sender=User)
def checkPioneerBadge(sender, user, request, **kwargs):
    # Quit if user already has pioneer badge
    if user.profile.pioneerBadge:
        return

    joined = user.date_joined.date()
    if date.today() > date(joined.year+1, joined.month, joined.day):
        messages.info(request, 'You won pioneer badge !!!')
        user.profile.pioneerBadge = True
        user.save()


@receiver(sig_model3d_displayed)
def checkStarBadge(sender, instance, **kwargs):
    # Quit if user already has star badge
    if instance.owner.profile.starBadge:
        return

    if instance.hitCount >= 1000:
        instance.owner.profile.starBadge = True
        instance.owner.save()


@receiver(sig_model3d_added)
def checkStarBadge(sender, instance, **kwargs):
    # Quit if user already has collector badge
    if instance.owner.profile.collectorBadge:
        return

    if len(Model3d.objects.filter(owner=instance.owner)) >= 5:
        instance.owner.profile.collectorBadge = True
        instance.owner.save()
