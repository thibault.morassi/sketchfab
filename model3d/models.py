from django.contrib.auth.models import User
from django.db import models

from model3d.signals import sig_model3d_displayed, sig_model3d_added


class Model3d(models.Model):
    title = models.CharField('Title', max_length=100)
    owner = models.ForeignKey(User, on_delete=models.CASCADE, blank=False)
    hitCount = models.IntegerField('hit count', default=0)

    def __str__(self):
        return self.title

    def sendStarBadgeCheckSignal(self):
        sig_model3d_displayed.send(sender=self.__class__, instance=self)

    def sendCollectorBadgeCheckSignal(self):
        sig_model3d_added.send(sender=self.__class__, instance=self)
