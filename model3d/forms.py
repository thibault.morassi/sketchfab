from django.forms import ModelForm

from model3d.models import Model3d

class Model3dForm(ModelForm):
    class Meta:
        model = Model3d
        fields = ('title',)

