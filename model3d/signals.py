import django.dispatch

sig_model3d_displayed = django.dispatch.Signal(providing_args=["instance"])
sig_model3d_added = django.dispatch.Signal(providing_args=["instance"])
