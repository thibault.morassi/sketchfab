from django.contrib.auth.decorators import login_required
from django.shortcuts import HttpResponse, get_object_or_404, render, Http404, redirect
from django.core.exceptions import PermissionDenied
from django.views import View

from model3d.models import Model3d
from model3d.forms import Model3dForm


def model3d_list(request):
    models = Model3d.objects.all()
    return render(request, 'model3d/list.html', context={'models': models})


def model3d_view(request, pk):
    model = get_object_or_404(Model3d, pk=pk)
    model.hitCount = model.hitCount + 1
    model.save()
    model.sendStarBadgeCheckSignal()
    return render(request, 'model3d/view.html', context={'model': model})


@login_required
def model3d_edit(request, pk=None):
    if pk:  # Edit an existing model
        model = get_object_or_404(Model3d, pk=pk)
        if model.owner != request.user:
            raise PermissionDenied
    else:  # Create a new model
        model=None

    if(request.method == 'POST'):
        form = Model3dForm(request.POST, instance=model)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.owner = request.user
            instance.save()

            # If it's a new model, check if owner won the collector badge
            if not model:
                instance.sendCollectorBadgeCheckSignal()

            return redirect('model3d_view', pk=instance.id)  # Success
        else:
            context = {
                'form': form,
                'pk': pk,
            }
            return render(request, 'model3d/edit.html', context)  # Form not valid
    else:
        if(model):
            form = Model3dForm(instance=model)
        else:
            form = Model3dForm()

        context = {
            'form': form,
            'pk': pk,
        }

        return render(request, 'model3d/edit.html', context)

