from django.conf.urls import url
from django.views.generic import TemplateView

from model3d import views


urlpatterns = [
    url(r'^$', views.model3d_list, name='model3d_list'),
    url(r'^model/(?P<pk>[0-9]+)/$', views.model3d_view, name='model3d_view'),
    url(r'^model/(?P<pk>[0-9]+)/edit/$', views.model3d_edit, name='model3d_edit'),
    url(r'^model/new/$', views.model3d_edit, name='model3d_new'),
]
