from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth import views as auth_views


handler403 = 'sketchfab.views.handler403'
handler404 = 'sketchfab.views.handler404'

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^login/$', auth_views.login, name='login'),
    url(r'^logout/$', auth_views.logout, {'next_page': '/'}, name='logout'),
    url(r'^profile/', include('userprofile.urls')),
    url(r'^', include('model3d.urls')),
]
