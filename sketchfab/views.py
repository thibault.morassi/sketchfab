from django.shortcuts import render

def handler403(request):
    response = render(request, '403.html')
    response.status_code = 403
    return response


def handler404(request):
    response = render(request, '404.html')
    response.status_code = 404
    return response

